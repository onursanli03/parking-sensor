package sample;


import eu.hansolo.enzo.gauge.OneEightyGauge;
import eu.hansolo.enzo.gauge.SimpleGauge;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import eu.hansolo.enzo.gauge.FlatGauge;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;



public class Controller implements Initializable{




    @FXML
    private OneEightyGauge frontGauge;

    @FXML
    private OneEightyGauge backGauge;

    private ScheduledExecutorService timer;

    @FXML
    private SimpleGauge simpleGauge1;

    @FXML
    private SimpleGauge simpleGauge2;

    String subData;

    double data;

    double data1,data2;


    ArduinoSerial arduino;

    int x = 1;
    int coreNumber = 0;


    public void init(){
        this.arduino =  new ArduinoSerial("COM6");
        this.subData = "";
        this.data = 0.0;
        this.data1 = 0.0;
        this.data2 = 0.0;
    }

    @FXML
    protected void readData(Event event){

        Runnable dataReaderFront = new Runnable() {
            @Override
            public void run() {

                arduino.initialize();

                while(true) {
                    subData = arduino.read();
                    if (subData.endsWith("o")) {
                        String[] dataFront = subData.split("o");
                        data1 = Double.parseDouble(dataFront[0]);
                        simpleGauge2.clockwiseProperty();
                        simpleGauge1.clockwiseProperty();
                        if (data1 > 5.0 && data1 <= 25.0) {
                            frontGauge.setBarColor(Color.RED);
                            frontGauge.setValue(data1);
                            simpleGauge1.setValue(25);

                        } else if (data1 > 25.0 && data1 <= 45.0) {
                            frontGauge.setBarColor(Color.BLUE);
                            frontGauge.setValue(data1);
                            simpleGauge1.setValue(50);
                        } else if (data1 > 45.0 && data1 <= 65.0) {
                            frontGauge.setBarColor(Color.YELLOW);
                            frontGauge.setValue(data1);
                            simpleGauge1.setValue(75);
                        } else if (data1 > 65.0 && data1 <= 100.0) {
                            frontGauge.setBarColor(Color.GREEN);
                            frontGauge.setValue(data1);
                            simpleGauge1.setValue(100);
                        }

                    } else if (subData.endsWith("a")) {
                        String[] dataBack = subData.split("a");
                        data2 = Double.parseDouble(dataBack[0]);
                        if (data2 > 5.0 && data2 <= 25.0) {
                            backGauge.setBarColor(Color.RED);
                            backGauge.setValue(data2);
                            simpleGauge2.setValue(25);
                        } else if (data2 > 25.0 && data2 <= 45.0) {
                            backGauge.setBarColor(Color.BLUE);
                            backGauge.setValue(data2);
                            simpleGauge2.setValue(50);
                        } else if (data2 > 45.0 && data2 <= 65.0) {
                            backGauge.setBarColor(Color.YELLOW);
                            backGauge.setValue(data2);
                            simpleGauge2.setValue(75);
                        } else if (data2 > 65.0 && data2 <= 100.0) {
                            backGauge.setBarColor(Color.GREEN);
                            backGauge.setValue(data2);
                            simpleGauge2.setValue(100);
                        }

                    }
                    x++;
                }


            }
        };

        this.timer = Executors.newSingleThreadScheduledExecutor();
        this.timer.scheduleAtFixedRate(dataReaderFront,0,50, TimeUnit.MILLISECONDS);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }
}
