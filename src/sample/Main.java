package sample;



import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sun.plugin2.util.NativeLibLoader;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/sample.fxml"));
            Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            primaryStage.setResizable(false);
            primaryStage.setTitle("Park Sensor");
            primaryStage.setScene(scene);
            primaryStage.show();

            Controller controller = loader.getController();
            controller.init();


        }catch (Exception e){
            e.printStackTrace();
        }


    }


    public static void main(String[] args) {

        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        launch(args);
    }
}
